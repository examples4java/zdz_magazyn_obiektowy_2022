package magazyn;
public class Util {
	static long zl_grosze(long kwota) {
		return zl_grosze((double)kwota);
	}

	static long zl_grosze(String kwota) {
		return zl_grosze(Double.valueOf(kwota.replace(',', '.')));
	}
	
	static long zl_grosze(double kwota) {
    	return (long)(kwota*100);
    }
}
