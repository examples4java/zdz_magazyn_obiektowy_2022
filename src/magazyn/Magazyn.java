package magazyn;
//import Pracownik;

public class Magazyn {
	
	
	class Towar {
		String nazwa,
		       numer,
		       opis;
		long cena;
		int ilosc_sztuk;		
		
		void setNazwa(String nazwa) {
			this.nazwa=nazwa;
		}
		
		String getNazwa() {
			return nazwa;
		}
		
		String getIlosc() {
			return String.valueOf(ilosc_sztuk);
		}
		
		void setCena(double kwota) {
			cena=Util.zl_grosze(kwota);
		}
		void setCena(long kwota) {
			cena=Util.zl_grosze(kwota);
		}
		void setCena(String kwota) {
			cena=Util.zl_grosze(kwota);
		}
		
		double getCena() {
			return (double)cena/100;
		}
		
		void setIlosc(int ilosc_sztuk) {
			this.ilosc_sztuk=ilosc_sztuk;
		}
		
		Towar setElement(String nazwa,Object setCena, int sztuki) {
			setNazwa(nazwa);
			setCena(String.valueOf(setCena));
			setIlosc(sztuki);
			//na koniec zwracamy przysz�y obiekt (pseudozmienna this)
			//jako pe�noprawny element naszego programu. Dzi�ki temu
			//mo�liwe stanie si� np. bezpo�rednie przypisanie warto�ci
			//Towar do jakiejkolwiek zmiennej nawet, gdy nie b�dziemy
			//bezpo�rednio tworzy� zmiennej Towar
			return this;
		}
	}
	
	//ponizej zmienne dost�pne jedynie dla klasy Magazyn. 
	//Pierwsza z nich (Towar[]) b�dzie przechowywa� wszystkie towary z naszego magazynu
	//druga - lokalizacja - b�dzie zawier� po�o�enie naszego magazynu danych.
 	
	Towar[] stan;	
	int index, currIndex;
	String lokalizacja;
	
	//to jest specjalna metoda  - KONSTRUKTOR. Zadaniem konstruktora jest
	//przy jego jego tworzeniu wywkonania np inicjalizacji wszystkich zmiennych,
	//kt�re b�d� nam p�zniej potrzebne w dalszym wykorzystaniu w programie.
	//Do chwili, gdy sami nie utworzymy takiego konstruktora (jak tu poni�ej)
	//Java tworzy tzw. konstruktor domy�lny,  na kt�ry sk�ada si� jego nazwa
	//oraz brak jakiekolwiek operacji. wygl�da on dok�adnie tak:
	// Magazyn() {}
	//stowrzenie jakiegokolwiek konstruktora przez programist� automatycznie
	//powoduje usuni�cie konstruktora domy�lnego
	//my, w naszym konstruktorze do�o�ymy inicjalizajc� 
	//naszej tablicy stan. Dzi�ki temu posini�ciu nie bedzie ona tworzona
	//w metodzie setTowar, co z kolei spowoduje,�e b�dzie ona stale
	//przechowywa� warto�ci od momentu utworzenia obiektu Magazyn
	//a� do jego zniszczenia w pami�ci (przez kolektor Java)
	public Magazyn() {
		stan = new Towar[100];
		index = 0;
		currIndex = 100;
	}
	
	public Magazyn(int vals) {
		stan = new Towar[vals];
		index = 0;
		currIndex = vals;
	}
	
	void resizeArray() {
		Towar tmp[] = new Towar[currIndex+100];
		for(int i=0;i<currIndex;i++)
			tmp[i] = stan[i];
		stan=tmp;
		currIndex+=100;
	}
	
	//metody publiczne pozwalaj�ce na dodanie do magazynu nowego towaru.
	public void setTowar() {
		setTowar(null, 0,0);
	}
	
	public void setTowar(String nazwa) {
		setTowar(nazwa, 0,0);
	}
	
	public void setTowar(String nazwa, double cena) {
		setTowar(nazwa, cena, 0);
	}
	
	//podstawowa metoda realnie dodaj�ce towar do magazynu.
	//Celem dodania nowego towaru musimy najpierw stowrzy� now� instancj�
	//(innymi s�owy now�, niezale�n� zmienn� wskazanego typu - w tym wypadku
	//zmienna typu Towar) klasy Towar, z kt�rej wybieramy metod� 
	//setElement (ustawia warto�ci obiektu na wskazane przez u�ytkownika
	//warto�ci
	public void setTowar(String nazwa, double cena, int sztuki) {
		//jednak ten spos�b dodania zako�czy si� niepowodzeniem
		//Zmienna zostanie stworzona, zostanie jej przypisane miejsce i 
		//wskazane waerto�ci po czym, zaraz przy zako�czeniu metody
		//setTowar ZNIKNIE Z PAMI�CI (bo nie ma do niej ju� innych odwo�a�
		//czy powi�za�)		
		new Towar().setElement(nazwa, cena, sztuki);
		
		//ROZWI�ZANIE PROBLEMU:
		//inicjalizujemy zmienn� tablicow�:
		//NOWY PROBLEM - stan jest inicjalizowany za ka�dym kolejnym wywo�aniem
		//metody setTowar; trzeba to wyeliminowa�
		
		//rozwi�zaniem by�o dodanie utworzenia tablicy w konstruktorze
		//stan = new Towar[10];
		
		//dodajemy na pierwszy indeks now� warto�� naszego towaru:
		//poni�ej kod rozbity na trzy linijki:
		//1 - utworzenie nowego obiektu typu Towar i przypisanie 
		//go do zmiennej tmp (nasz tzw. uchwyt do obiektu)
		//2 - wykonanie metody setElement, kt�ra powoduje 
		//ustawienie okre�lonych warto�ci naszego towaru
		//3 - przypisanie tak utworzonej zmiennej (wraz z zawarto�ci�)
		//do pierwszego elementu w w naszej tablicy
		Towar tmp = new Towar();
		tmp.setElement(nazwa, cena, sztuki);
		//KOLEJNY PROBLEM - statyczne przypisanie indeks�w, do kt�rych
		//dodajemy nasze nowe towary na magazyn
		//stan[0] = tmp;
		//rozwi�zanie - doda� zmienn� index zamiast litera�u i 
		//wykorzysta� postinkrementacj� celem automatycznego przeskoczenia
		//zawarto�ci index na kolejn� warto�� (np. z 0 na 1, z 1 na 2 itp.)
		if (index == currIndex-1)
			resizeArray();
		stan[index++] = tmp;
		
		//w przypadku, gdy do setElement dodali�my return this oraz
		//ndali�my metodzie typ zwracany (zwraca tak naprawd� element Towar,
		//na kt�rym obecnie pracujemy)
		//mo�liwe sta�o si� wywo�anie i przypisanie nowego elementu dos�ownie
		//w jednej linii - utworzenie nowego obiektu w pami�ci, przypisanie 
		//mu warto�ci poprzez wywo�anie metody, za� metoda zwraca ten obiekt
		//bezpo�rednio do drugiego indeksu w tablicy (i on w zasadzie
		//staje si� "uchwytem" do nowej warto�ci)
		//stan[1] = new Towar().setElement(nazwa, cena, sztuki+1);
	    //jak przy poprzednim zapisie:
		if (index == currIndex-1)
			resizeArray();
		stan[index++] = new Towar().setElement(nazwa, cena, sztuki+1);		
	}
	
	public String getTowar(int id) {
		String ret = stan[id].getNazwa() + " " + stan[id].getCena() + " " + stan[id].getIlosc();
		return ret;
	}
}
