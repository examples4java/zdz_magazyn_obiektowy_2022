import java.util.Scanner;


import magazyn.*;


public class Main {

		
		
	public static void main(String[] args) {
		//r�ne sposoby tworzenia zmiennej na podstawie posiadanych klas
		//poni�ej spos�b klasyczny skr�cony - deklarujemy, �e chcemy utworzy�
		//zmienn� m b�d�c� typem Magazyn, po czym inicjalizujemy j� poprzez
		//u�ycie operatora new, kt�ry wskazuje systemowi operacyjnemu 
		//by zagospodarowa� odpowiedni� ilo�� miejsca w pami�ci operacyjnej
		//na now� zmienn�. 
		Magazyn m = new Magazyn(1);
		//teraz mo�emy u�y� dowolnej sk�adowej z naszego magazynu. Poniewa�
		//klasa Magazyn znajduje si� w innej paczce ni� nasza obecna klasa Main
		//(main jest w g��wnej cz�ci projektu, klasa Magazyn i Util zosta�y
		//przeniesione do paczki/podfolderu magazyn) sk�adowe MUSZ� BYC TYPU 
		//PUBLIC by mo�na by�o ich u�y� w kodzie poni�ej (gdyby by�y w tej samej paczce
		//- lokalizacji, problem by nie istnia�!)
		m.setTowar("Klawiatura");
		m.setTowar("Mysz");
		System.out.println(m.getTowar(3));
		System.out.println(m.getTowar(1));
		System.out.println(m.getTowar(2));
		System.out.println(m.getTowar(0));
		
		//Osoba p = new Osoba();
	}

}
